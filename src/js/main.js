
$(function(){
    $('#search').autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: 'http://www.omdbapi.com?s='+ request.term +'&apikey=daee70b3',
                dataType: 'json',
                data: {
                    movie:request.term
                },
                success: function( data ) {
                    let movies = [];
                    jQuery.each(data.Search, function(index, item) {
                        let imdb = item.imdbID;
                        $.ajax({
                            url: 'http://www.omdbapi.com?i='+ imdb +'&apikey=daee70b3',
                            dataType: 'json',
                            data: {
                                movieDetail:imdb
                            },
                            success: function (data) {
                                movies.push(data);
                                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                    response(movies.slice(0, 1));
                                } else {
                                    response(movies.slice(0, 2));
                                }
                            }
                        });
                    });
                }
            });
        },
        open: function(event,ui){
            let len = $('.ui-autocomplete > li').length;
            $('#count').html( len + ' film bulundu');

            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                if(len >= 1) {
                    var resultID = document.getElementById('ui-id-1');
                    resultID.innerHTML += '<button class="showAll" onclick="showAll()">DAHA FAZLA SONUÇ</button>';
                }
            } else {
                if(len >= 2) {
                    var resultID = document.getElementById('ui-id-1');
                    resultID.innerHTML += '<button class="showAll" onclick="showAll()">DAHA FAZLA SONUÇ</button>';
                }
            }
        },
        minLength: 1
    });
    
    $('#search').data('ui-autocomplete')._renderItem = function( ul, item ){
        let re = new RegExp("^" + this.term, "gi");
        let t = item.Title.replace(re,"<span style='font-weight: bold;text-decoration: underline;text-transform: capitalize;'>" + this.term + "</span>");

        let $li = $('<li class="movie row">');
        $li.html(
            '<figure class="col-sm-12 col-md-12 col-lg-4 col-xl-4 p-0">' +
                '<img class="img-fluid" src="' + item.Poster + '" />' +
            '</figure>' +
            '<div class="movie-info col-sm-12 col-md-12 col-lg-8 col-xl-8">' +
                '<div class="title">' +
                    '<h2>' + t + ' <label>(' + item.Year + ')</label></h2>' +
                    '<span class="imdb"><div class="star-icon"></div> <b>' + item.imdbRating + '</b>/10</span>' +
                '</div>' +
                '<span><strong>Dil:</strong>' + item.Language + '</span>' + 
                '<span><strong>Oyuncular:</strong> ' + item.Actors + ' | <a target="_blank" href="https://www.imdb.com/title/' + item.imdbID + '/fullcredits?ref_=tt_cl_sm#cast">Tüm listeyi gör</a></span>' +
                '<span class="plot">' +
                    '<p>' + item.Plot + '</p>'+
                    '<a target="_blank" href="https://www.imdb.com/title/' + item.imdbID + '/?ref_=inth_ov_tt">Detaylar</a>' +
                '</span>' +
            '</div>'
        );
        return $li.appendTo(ul);
    ;}

    $("#searchForm input").on('focus blur', function(){
        $(this).parent().toggleClass('focused');
        $(".ui-autocomplete").parent().toggleClass('focused');
    });
});

window.showAll = function() {   
    let showAll = $("[name='search']").val();
    sessionStorage.setItem('showAll',showAll);
    window.location = 'movie-list.html';
    return false;
}

window.pageUp = function() {
    $('html,body').animate({scrollTop: $("#searchForm").offset().top},'slow');
}

window.getMovie = function() {   
    let showAll = sessionStorage.getItem('showAll');

    $('input[name=search]').attr("placeholder", showAll + " için Sonuçlar");

    $.ajax( {
        url: 'http://www.omdbapi.com?s='+ showAll +'&apikey=daee70b3',
        dataType: 'json',
        data: {
            movie: showAll
        },
        success: function( data ) {
            let movies = [];
            jQuery.each(data.Search, function(index, item) {
                let imdb = item.imdbID;
                $.ajax({
                    url: 'http://www.omdbapi.com?i='+ imdb +'&apikey=daee70b3',
                    dataType: 'json',
                    data: {
                        movieDetail:imdb
                    },
                    success: function (data) {
                        movies.push(data);
                        let output = '';
                        $.each(movies, (index, movie) => {
                            let re = new RegExp("^" + showAll, "gi");
                            let t = movie.Title.replace(re,"<span style='font-weight: bold;text-decoration: underline;text-transform: capitalize;'>" + showAll + "</span>");

                            output += `
                                <div class="movie col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 d-flex">
                                    <figure class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 p-0">
                                        <img src="${movie.Poster}" class="img-fluid">
                                    </figure>
                                    <div class="movie-info col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="title">
                                            <h2>${t} <label>(${movie.Year})</label></h2>
                                            <span class="imdb"><div class="star-icon"></div> <b>${movie.imdbRating}</b>/10</span>
                                        </div>
                                        <span><strong>Dil:</strong>${movie.Language}</span>
                                        <span><strong>Oyuncular:</strong> ${movie.Actors} | <a target="_blank" href="https://www.imdb.com/title/${movie.imdbID}/fullcredits?ref_=tt_cl_sm#cast">Tüm listeyi gör</a></span>
                                        <span class="plot">
                                            <p>${movie.Plot}</p>
                                            <a target="_blank" href="https://www.imdb.com/title/${movie.imdbID}/?ref_=inth_ov_tt">Detaylar</a>
                                        </span>
                                    </div>
                                </div>
                            `;
                        });
                        output += `<a class="pageUP" onclick="pageUp()" href="javascript:void(0);">BAŞA DÖN</a>`;
                        $('#movies').html(output);
                        let len = $('#movies > .movie').length;
                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            $('#count').html('(' + len + ')');
                        } else {
                            $('#count').html(len + ' film bulundu');
                        }
                    }
                });
            });
        }
    });
}