# Works (Sketch Demo)

Film arama demo projesi.

## Kurulum

Paket kurulumları için aşağıdaki komutu kullanabilirsiniz;

```
npm i
```

### Build

Projenin build işlemi için;

```
npm run build
```
